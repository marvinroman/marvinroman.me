+++
title = "About Me"
slug = "about"
thumbnail = "images/tn.png"
description = "about"
+++

---------------------------
A software engineer specializing in Netsuite Development.

---------------------------

Current work
* Creating logistics automations for a Food Bank.

Past Lives 
* IT Executive for a Solar Company.
* Systems Architect for a Hosting Company.
* Developer for e-commerce stores.