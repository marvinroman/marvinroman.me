---
date: "2022-03-09"
title: "Render Multi-page PDF in Netsuite"
tags: 
    - "json"
    - "pdf"
    - "netsuite"
    - "suitescript"
categories: 
    - "Development"
---

- [Terms](#terms)
- [Problem](#problem)
- [Solution](#solution)
  - [Create a Suitelet to Select Agencies](#create-a-suitelet-to-select-agencies)
  - [Example Code Snippets](#example-code-snippets)

## Terms

* **Agency** - Customer record that represents the institution that is distributing the food.  

* **Client** - Custom record with a parent of Agency that represents the final person receiving products.  

* **Site** - Synonomous to Agency.  

## Problem

* Print out a roster for Clients to sign for all Agencies.  
* Each Agency gets it's own roster, but should be part of 1 large PDF document to print out.  

## Solution

To see screenshots of the final product see the project [Senior Brown Bag Roster](https://dev.marvinroman.me/projects/#senior-brown-bag-roster).  

### Create a Suitelet to Select Agencies

* Use *Suitelet* *Suitescript* type to create a custom page within Netsuite.  

* GET request to the suitelet script delivers an HTML framework built with Vue Bootstrap in order to deliver a custom page to the user.  

* Suitelet allows the user to choose some filters will determine the **Agencies** that are able to be chosen to create rosters for.  

* User multi-selects the **Agencies** to print rosters for and then hits the **Submit** button and the roster is rendered and delivered back to the user to print out.  

* POST request to the suitelet script when the user uses **Submit** is what renders the PDF and delivers back to the user.  

### Example Code Snippets

* Create a template file with all the PDF content within the `<pdf>` tags.  

* Use Freemarker to merge JSON data into the template.  

```html
<head>
    <!-- Head Content -->
</head>
<body>
<!-- pre list content -->
    <table class="patrons">
        <#list site.patrons as patron>
            <#if patron?is_first>
                <thead>
                    <tr class="top-row">
                        <th>${patron.lname.label}</th>
                        <th>${patron.fname.label}</th>
                        <th>${patron.phone.label}</th>
                        <th>${patron.number_hh.label}</th>
                        <th>${patron.client_apt_number.label}</th>
                        <th class="signature">Signature</th>
                    </tr>
                </thead>
            </#if>
            <tr>
                <td class="first-col">${patron.lname.value}</td>
                <td>${patron.fname.value}</td>
                <td>${patron.phone.value}</td>
                <td align="center">${patron.number_hh.value}</td>
                <td>${patron.client_apt_number.value}</td>
                <td class="signature"></td>
            </tr>
        </#list>
    </table>
<!-- post list content -->
</body>
```  

* Outside of the following snippet an Object with **Sites** is built with **Clients** as children records.  

* The template to use for rendering is chosen within the suitelet and passed to the script.  

* Load in the template file.  

* Loop through **Sites** and for each **Site** render template and add JSON objects to be merged into the data using Freemarker.  

* For each loop concatenate the final rendered template as a string `pdf_contents`.  

* After the loop add the `pdf_contents` within the doctype and pdf elements and write out the pdf.  

```js
// load template file
const template_file = file.load({id: body_parameters.report.template_path});
// initialize string to concatenate the rendered templates into
let pdf_contents = '';
// loop through sites
for (const site_key in sites) {

    // create template renderer and push template contents into it
    const renderer = render.create();
    renderer.templateContent = template_file.getContents();

    // add data source content that are the same across all pages
    renderer.addCustomDataSource({
        alias: "JSON",
        format: render.DataSource.JSON,
        data: JSON.stringify(pdf_json)
    });

    // add data source that is specific for this site
    renderer.addCustomDataSource({
        alias: "site",
        format: render.DataSource.JSON,
        data: JSON.stringify(sites[site_key])
    });

    // use renderer to create an xml string and concatenate it into a variable
    pdf_contents += renderer.renderAsString();
}

// convert xml string into pdf
const report_pdf = render.xmlToPdf({
    xmlString: `<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd"><pdf>${pdf_contents}</pdf>`
});

// write out file to suitelet
scriptContext.response.writeFile({
    file: report_pdf,
    isInline: false
});
```  
