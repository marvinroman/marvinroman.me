---
date: "2022-02-26"
title: "Distribution / Incoming Calendar"
tags: 
    - "vue"
    - "quasar"
    - "fullcalendar"
    - "netsuite"
    - "suitelet"
    - "suitescript"
    - "restlet"
categories: 
    - "Development"
    - "Projects"
---

- [Problem](#problem)
- [Solution](#solution)
- [Pages](#pages)
  - [Permissions](#permissions)
  - [Distribution Calendar](#distribution-calendar)
    - [Filtering](#filtering)
    - [Staging Changes](#staging-changes)
    - [Periodic Refresh](#periodic-refresh)
    - [Appointment Colors/Icons](#appointment-colorsicons)
    - [Popup Modal](#popup-modal)
  - [Incoming Calendar](#incoming-calendar)
    - [Filtering](#filtering-1)
    - [Adding Appointments](#adding-appointments)
    - [Colors and Icons](#colors-and-icons)
- [Build](#build)
  - [Netsuite Suitlet](#netsuite-suitlet)
  - [Netsuite RESTlets](#netsuite-restlets)
  - [Quasar Framework](#quasar-framework)
  - [FullCalendar](#fullcalendar)
  - [How is it delivered?](#how-is-it-delivered)
- [Dependencies](#dependencies)
- [Repository](#repository)

## Problem

* Provide two calendar views for Inventory moving in and out of the foodbank.  
* Allow users drag and drop capability in order to reschedule appointments.  
* Provide capability to update fields on multiple records related to appointments.  
* Provide ability to filter the visible appointments.  

## Solution

## Pages

### Permissions

All signed in users are able to see the calendars as long as they have access to view the underlying records.  
Edit permissions are given by role to each calendar using the Suitelet deployment script parameters.  

### Distribution Calendar

{{< figure src="/images/distribution-calendar-dashboard.png" class="floating-image md float-right" >}}  
Provides a calendar that interacts with Sales Orders and appointment records that hold information for distributions going out.
{{< spacer >}}

#### Filtering

{{< figure src="/images/distribution-calendar-filtering.png" class="floating-image sm float-left" >}}  
Allows for filtering incoming appointments based on Sales Order Warehouse, Agency Program, or Sales Order Shipping Method. A user has the ability to save their default filters back to their employee record so that evertime they enter the calendar they receive the same default filters.
{{< spacer >}}

#### Staging Changes

{{< figure src="/images/staging-changes-submit-button.png" class="floating-image xsm float-right" >}}
{{< figure src="/images/refresh-button.png" class="floating-image xsm float-right" >}}
To reduce traffic back and forth to the Netsuite RESTlets and give the user to move many appointments and review before submitting the bulk changes. As soon as an appointment is changed the **Refresh** button changes to a **Save Changes** button with a dropdown option to **Refresh** in case the user wants to revert the changes.

After submitting bulk changes the appointments on the calendar will be refreshed to make sure it represents all recent updates.  
{{< spacer >}}

#### Periodic Refresh

The calendar will refresh the appointments on the calendar periodically to make sure if it is open for a period of time that the appointments being shown are up to date. This periodic refresh is stopped as soon as the user starts changing appointments on the calendar and won't start again until they are submitted or the calendar is refreshed.  

#### Appointment Colors/Icons

* The colors vary in the calendar based on whether it has been assigned a driver.  
* The icons on the appointment vary based on whether it will be delivered or picked up.  

#### Popup Modal

{{< figure src="/images/distribution-calendar-popup.png" class="floating-image sm float-left" >}}  
Shows in depth information that comes from the appointment record, Agency/Customer record, and Sales Order record.  Some information can be updated within the popup and will be updated along with the other appointment changes when the user clicks the button **Save Changes**.  
{{< spacer >}}

### Incoming Calendar

{{< figure src="/images/incoming-calendar-screenshot.png" class="floating-image md float-right" >}}  
Provides a calendar that interacts with Purchase Orders, Vendors, and Appointments within a calendar.  Allows for the adding and manipulating of scheduled warehouse deliveries.  
{{< spacer >}}

#### Filtering

{{< figure src="/images/incoming-calendar-filtering.png" class="floating-image sm float-left" >}}  
Allows for filtering by Warehouse, Created By, Product Category, or Vendor.  The user can save the filters to their employee record in order to default those filters when they enter the calendar again.
{{< spacer >}}

#### Adding Appointments

{{< figure src="/images/incoming-calendar-add-appointment.png" class="floating-image sm float-right" >}}
By clicking on a time on the calendar a popup will show that allows you to add a new appoinment to the calendar. You can start to add in information or save with just a Vendor in which case it will be considered a HOLD and will show up yellow on the calendar.  
{{< spacer >}}
{{< figure src="/images/vendor-dynamic-search.png" class="floating-image xsm float-left" >}}
As you type in the vendor field it will dynamically search Vendor records and return options that match.  
{{< spacer >}}
{{< figure src="/images/purchase-order-dynamic-search.png" class="floating-image xsm float-right" >}}
As you type in the Purchase Order field it will return the Purchase Orders that match. If you have a Vendor selected the returned Purchase Orders will be limited to those for that Vendor.  
{{< spacer >}}

#### Colors and Icons

{{< figure src="/images/hold-appointment.png" class="floating-image xsm float-right" >}}

* Yellow background signifies a HOLD which serves to hold a time for a delivery before there is a Purchase Order created.
{{< spacer >}}
{{< figure src="/images/unapproved-purchase-order-appointment.png" class="floating-image xsm float-right" >}}
* Grey background signifies a Purchase Order that is not yet approved.  
{{< spacer >}}
{{< figure src="/images/full-delivery-appointment.png" class="floating-image xsm float-right" >}}
* Green background signifies a valid Purchase Order exists.
{{< spacer >}}
{{< figure src="/images/full-delivery-appointment-w-produce.png" class="floating-image xsm float-right" >}}
* Orange carrot icon signifies an order that includes produce.
{{< spacer >}}
{{< figure src="/images/full-delivery-appointment.png" class="floating-image xsm float-right" >}}
* Solid truck icon signifies a full delivery which will span an hour slot.
{{< spacer >}}
{{< figure src="/images/full-delivery-appointment-w-carrier.png" class="floating-image xsm float-right" >}}
* Solid truck icon with check signifies a full delivery with carrier information filled in.  
{{< spacer >}}
{{< figure src="/images/small-delivery-appointment.png" class="floating-image xsm float-right" >}}
* Truck with outline is a small delivery which will span a half hour slot.  
{{< spacer >}}
{{< figure src="/images/small-delivery-appointment-w-carrier.png" class="floating-image xsm float-right" >}}
* Truck with outline and check signifies a small deliver with carrier information filled in.  
{{< spacer >}}

## Build

### Netsuite Suitlet

The calendars are delivered to the user through a Netsuite Suitelet script type. Using a Suitelet to deliver the calendar allows us to make sure it is delivered only to authenticated Netsuite users.

### Netsuite RESTlets

To build in future capabilities to be able to host the calendar outside Netsuite and still be able to interact with the Netsuite records it's interactions with Netsuite records are delivered through Restlet API's.

### Quasar Framework

The UI is built with the Quasar Framework for the UI components outside the calendar. This was chosen to give future ability to deploy as a desktop app or mobile app in the future disconnected from Netsuite except for calls to the RESTlets.

### FullCalendar

The calendar library being used is FullCalendar to allow for drag and drop capability and different calendar views. It has a Non-Commercial license to allow Registered Non-profits to use the calendar framework for free.

### How is it delivered?

* A Netsuite Suitelet script gets the contents from an HTML template file and writes that content out to the browser instead of a Netsuite page.  
* Quasar is compiled to be a single page app (SPA), and with some webpack configurations packs most all Javascript and CSS into the single HTML file.  
* There are still a few external JS & CSS resource files that are included in the SPA file, so the Suitelet script finds these entries using a regex. Since Netsuite does not deliver resources using relative URL's we have to rewrite the paths based on Netsuite URLs. So if there are resource paths that are relative and not Netsuite paths then we search for each of these files and rewrite them in the HTML and resave the HTML file so this only happens after new packages are uploaded. Then the HTML is delivered to the client to build out the calendar pages.  
* Quasar which is based in Vue which modifies the UI as data comes in from Restlet calls.

## Dependencies

* Requires a custom record that will hold appointment information and be linked from both Sales Orders and Purchase Orders.
  * Current one includes an appointment that is deployed from MHI's foodbank software, another appointment record is custom to the client.  
* Currently some of the filtering is done on segments that are not standard to Netsuite. This can be adjusted to segments relivent to the client.  
* Currently some of the fields that are updated aren't Netsuite standard fields, they can be added or altered based on clients needs.  

## Repository

This project is currently held in a private repository, but is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) so please contact me in order to release a public version if your interested in utilizing.
