---
date: "2022-03-09"
title: "Site Goes into Redirect Loop When Behind Loadbalancer/Reverse Proxy"
tags: 
    - "traefik"
    - "nging"
    - "apache"
    - "php"
    - "https"
    - "redirect loops"
categories: 
    - "Devops"
    - "System Administrator"
---

- [How to fix in NGINX](#how-to-fix-in-nginx)
- [How to fix in Apache](#how-to-fix-in-apache)

If after you put your site behind a loadbalancer/proxy, like Traefik or NGINX, your site goes into a redirect loop. This can be because the reverse proxy is communicating the backend server on HTTP even if it terminates with the clients browser on HTTPS. This means that the backend webserver won't pass on the header `HTTPS=true` to PHP. So if your software has a rule to redirect HTTP to HTTPS this will cause a rediret loop to occur.  

Best practice is to use webserver rules to redirect HTTP to HTTPS and to turn off the redirect in your site software, but if you don't want to turn this rule off or don't know how to then you can ensure that the proper header is sent to PHP from the webserver.

## How to fix in NGINX

Normally the `HTTPS` header is sent to PHP in the `/etc/nginx/fastcgi_params` file like `fastcgi_param HTTPS $https;` which passes the NGINX variable `$https` to `HTTPS`. To overwrite this you will want to go to your virtualhost configuration to the php section which will look similar to below and insert `fastcgi_param HTTPS On;` right after `include fastcgi_params;`.  

```nginx
location ~ [^/]\.php(/|$) {

    fastcgi_pass 127.0.0.1:9000;
    fastcgi_index index.php;

    # include the fastcgi_param setting
    include fastcgi_params;
    fastcgi_param  HTTPS On;
}
```  

## How to fix in Apache

For Apache there are two options to set the `HTTPS` variable that gets passed as a `$_SERVER` environment information.  

The first is allowing it to be set dynamically based on environment variables within the container. In the below example the environment variable is `BEHIND_LOADBALANCER` and if it is set to the string value `true` then set `HTTPS` to `On`.  

```ApacheConf
<VirtualHost *:80>
    ServerName www.example.com

    ServerAdmin ${ADMIN_EMAIL}
    DocumentRoot ${ROOT_WEB}

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    SetEnvIf BEHIND_LOADBALANCER "true" HTTPS On
</VirtualHost>
```  

The second is by statically setting `HTTPS` to `On` always in the configuration.  

```ApacheConf
<VirtualHost *:80>
    ServerName www.example.com

    ServerAdmin ${ADMIN_EMAIL}
    DocumentRoot ${ROOT_WEB}

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    SetEnv HTTPS On
</VirtualHost>
```
