---
date: "2022-03-12"
title: "Multi Order Suitelet"
tags: 
    - "vue"
    - "bootstrap"
    - "netsuite"
    - "suitelet"
    - "suitescript"
categories: 
    - "Development"
    - "Projects"
---

- [Problem](#problem)
- [Solution](#solution)
  - [Screenshots](#screenshots)
    - [Multi Order Record Example](#multi-order-record-example)
    - [Suitelet Example](#suitelet-example)
- [Record Types Used](#record-types-used)
  - [Custom Records Created](#custom-records-created)
  - [Netsuite Records](#netsuite-records)
- [Licensing](#licensing)

## Problem

Give Program Managers the ability to create Batch Orders that will span multiple **Agencies**{{< superscript number=1 >}} at once.  

## Solution

* Create a Multi Order record that will hold the information that will be consistent across orders and to filter which agencies that will be potentials for the order.  

* Create a Suitelet that will deliver the filtered agencies and summarized orders for each Agency.  

* Create automatic calculations that will be distributed to each Agency based on the amount of people the Agency serves.  

    * **Bulk calculation:** The total amount of items available for distribution are included on the Multi Order. The total items available will be distributed across all selected Agencies based on the amount of people the Agency served as a percentage of the total served by the entire order.  

    * **Each calculation:** The items added to the multi order will be for the quantity to be allocated to each person served. The amount allocated to each Agency will be the quantity of each item multiplied by the amount of people served by the Agency.  

* Create a way for the Program Manager to initiate an order, be able to save it and then come back before committing it for creation.  

* Allow for Program Manager to come back and make some changes to all orders until orders reach a certain status.  

* Allow for the cancling of all orders attached held under a Multi Order. Close all Sales Orders, Purchase Orders, and make the Internal Appointments Inactive.  

* Allow for some items on the order to be marked as Special Order, bypassing the warehouse and being delivered directly to the Agency from the Vendor.  

* Lock the Multi Order from further changes once any Item Fulfillments happen or Ship Dates are within 3 business days.  

* Allow for Full Pallet allocation which allocates a full pallet to each Agency.  

* Rollup all total numbers to help the Program Manager understand the total allocations across all Agencies.  

### Screenshots

#### Multi Order Record Example

![Multi Order Record Screenshot](/images/multi-order-record-screenshot.png)  

#### Suitelet Example

![Multi Order Suitelet Screenshot](/images/multi-order-suitelet-screenshot.png)  

## Record Types Used

### Custom Records Created

* **Multi Order**  - Stores specific batch order information that will be delivered to the suitelet.  

* **Multi Order Item** - Child of Master Order and keeps item specific information.  

* **Internal Appointment** - Record attached to the Sales Order and holds distribution date, time & location.  

### Netsuite Records

* **Customer** - Renamed to Agency.  

* **Sales Order**  

* **Purchase Order**  

## Licensing

This project is currently held in a private repository, but is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) so please contact me in order to release a public version if your interested in utilizing.

{{< spacer color=gray >}}  

1. Agencies are customer records and represent approved organizations that aid in the distribution of food.  
