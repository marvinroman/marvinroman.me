---
date: "2022-04-30"
title: "Waerlinx Custom Status Dashboards"
tags: 
    - "waerlinx"
    - "laravel"
    - "vue"
    - "bootstrap"
    - "e-charts"
categories: 
    - "Vue-Bootstrap"
    - "E-Charts"
---

- [Problem](#problem)
- [Solution](#solution)
- [Dashboards](#dashboards)
  - [Picking Dashboard](#picking-dashboard)
    - [Picking Dashboard Layout](#picking-dashboard-layout)
    - [Picking Dashboard Filtering](#picking-dashboard-filtering)
  - [Inventory Counts Dashboard](#inventory-counts-dashboard)
    - [Inventory Counts Dashboard Layout](#inventory-counts-dashboard-layout)
    - [Inventory Counts Filters](#inventory-counts-filters)
  - [Driver Dashboard](#driver-dashboard)
    - [Drivers Dashboard Layout](#drivers-dashboard-layout)
    - [Drivers Dashboard Filters](#drivers-dashboard-filters)
- [Licensing](#licensing)

## Problem

* Food Bank uses Waerlinx as their Wharehouse Management system.  
* They have access to a mirror of their production database.  
* Waerlinx provides some views for running reports but no database schema for their tables.  
* The Food Bank had the desire to see live picking data in their warehouse displayed on a monitor.  
* The Food Bank also wanted to have some dashboard for live reporting purposes on inventory movements.  

## Solution

* Create Waerlinx queries that will return the desired data.  
* Use Laravel to interact with the Waerlinx database and return the dashboards.  
* Use Vue Bootstrap to control user interactions and provide the page framework.  
* Use E-Charts to display charts.  

## Dashboards

### Picking Dashboard

Meant to be displayed on a monitor within the warehouse for the pickers to view the status of the days picks.  

#### Picking Dashboard Layout

{{< figure src="/images/picking-dashboard-screenshot.png" >}}  

{{< spacer >}}

The ring graph displays how many orders have been processed and are in each of the major statuses.  

The bar graph display how many orders have been processed by each employee.  

The table displays each order in more detail.  

* How many different locations are required to pick from and by expanding seeing the item being picked from each location.  
* How many total items are in the order.  
* The employee assigned to pick the order.  
* Ship method of the order.  
* Status of the order.  

{{< figure src="/images/picking-dashboards-filter-screenshot.png" class="floating-image sm float-left" >}}  

#### Picking Dashboard Filtering

* The user can filter by days forward 3 days and 10 days in the past to see the dashboard for each of those days.  
* The user can filter by shipping methods.  
* The bottom section allows the user to create a kiosk url that will not have a menu and use the filtering selected by the user.  

{{< spacer >}}

### Inventory Counts Dashboard

Each location is supposed to be counted 2 times per month. This dashboard tracks the counts done in each location.  

#### Inventory Counts Dashboard Layout

{{< figure src="/images/inventory-counts-dashboard-screenshot.png" >}}  

The ring graph shows the total counts for all locations within the filtered area, by first, second and total.  

The bar graphs shows how many counts have been done per employee.  

The table shows more detailed totals per rack.  

* Total locations within the rack.  
* Total locations that have gone through the first count for the month.  
* Total lcoations that have gone through the second count for the month.  
* By expanding a rack you can see each location and the dates of the counts.  

{{< figure src="/images/inventory-counts-filters-screenshot.png" class="floating-image sm float-left" >}}  

#### Inventory Counts Filters

* Pick a date for start and end.  
* Pick a warehouse.  
* Pick a zone within the selected warehouse.  
* Pick a rack within the selected zone.  
* The bottom section allows the user to create a kiosk url that will not have a menu and use the filtering selected by the user.  

{{< spacer >}}

### Driver Dashboard

Shows the status of orders that are due to be delivered and their status.    

#### Drivers Dashboard Layout

{{< figure src="/images/driver-dashboard-screenshot.png" >}}  

The ring graph shows how many orders have been scanned onto trucks and scanned off trucks for the day.  

The bar graph shows how many orders are assigned and delivered by each driver.  

The table shows each order in detail.  

* The order number.  
* The agency that the order will be delivered to.  
* The shipping method for the order.  
* How many pallets in total for the order.  
* The driver assigned to deliver the order.  
* The current status of the order.  
* By expanding the order the user can see:  
  * The address the order is being delivered to.  
  * All items and their pallet tracking number.  

{{< figure src="/images/driver-dashboard-filters-screenshot.png" class="floating-image sm float-left" >}}  

#### Drivers Dashboard Filters

* User can filter days relative to today forward 1 day and back 10 days.  
* User can filter by shipping methods.  
* The bottom section allows the user to create a kiosk url that will not have a menu and use the filtering selected by the user.  

{{< spacer >}}

## Licensing

This project is currently held in a private repository, but is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) so please contact me in order to release a public version if your interested in utilizing.

{{< spacer >}}