+++
title = "Projects"
slug = "projects"
description = "projects"
+++

## [Distribution Scheduling Calendar](/posts/distribution-incoming-calendar/)

{{% portfolio image="/images/distribution-calendar-dashboard.png" alt="Distribution Calendar Screenshot" %}}

## [Distribution Calendar](/posts/distribution-incoming-calendar/#distribution-calendar)

* Drag and Drop updating of outgoing distributions.  

* Ability to stage changes and submit as bulk updates.  

* Ability to filter the visible appointments by Warehouse, Program, Shipping Method.  

* Popup that shows appointment and Sales Order data in depth and ability to update selected information.  
{{% /portfolio %}}

{{% portfolio image="/images/incoming-calendar-screenshot.png" alt="Incoming Calendar Screenshot" %}}

## [Incoming Calendar](/posts/distribution-incoming-calendar/#incoming-calendar)

* Drag and Drop updating of incoming distributions.  

* Ability to create incoming distributions and search and link to Purchase Orders.

* Ability to filter visible appointments by Warehouse, Person that Created, Product Category, Vendor.  

* Appointments can be queued as all-day and then dragged to a specific time.  

* Popup has in depth information on the appointment, vendor, & Purchase Order and the ability to update data.  

{{% /portfolio %}}

{{< spacer >}}

## Senior Brown Bag Roster

{{% portfolio image="/images/sbbr-w-selected-agencies-screenshot.png" alt="Senior Brown Bag Suitelet Screenshot" %}}

## Suitelet

* Choose from different PDF roster templates.  

* Choose applicable Fiscal Year to filter Clients available for distributions during that fiscal period.  

* Set distribution month to print on the roster.  

* Multi-select the Agencies/Customers that distribute the food to the final Client.  

{{% /portfolio %}}

{{% portfolio image="/images/sbbr-roster-screenshot.png" alt="Senior Brown Bag Roster Screenshot" %}}

## Roster

* Prints out multi-page PDF roster for submission to the USDA.  

{{% /portfolio %}}

{{< spacer >}}

## [Multi-Order Suitelet](/posts/multi-order-suitelet-project/)

{{% portfolio image="/images/multi-order-suitelet-screenshot.png" alt="Multi-Order Suitelet" %}}

## Suitelet

* Allows the users to create mass orders for Agencies.  

* Amounts are allocated in one of two ways.  

    * **Bulk** ordering: before entering the suitelet the quantity available for allocation are added to the items.  The suitelet will distribute those quantities based on how many people the agency serves.  

    * **Each** ordering: before entering the suitelet the quantity each person served should receive is added to the items.  The suitelet will multiply each item quantity by the number of people served.  

* Agencies can be selected and deselected in order to create orders for them.  

* The order can be saved without completing and come back. Settings are saved back to the Multi-Order record as a JSON object and will be retrieved next time they enter the suitelet.  

* When selecting Save & Create the status of the record Multi Order will be set to **Pending Creation** and a scheduled script will begin creating the Sales Orders.  

{{% /portfolio %}}


## [Waerlinx Dashboards](/posts/waerlinx-custom-status-dashboards/)

{{% portfolio image="/images/waerlinx-dashboards.png" alt="Waerlinx Dashboards" %}}

## Dashboards

* **[Picking Dashboard](/posts/waerlinx-custom-status-dashboards/#picking-dashboard)** - meant to be displayed on a monitor within the warehouse for the pickers to view the status of the days picks.  

* **[Inventory Counts Dashboard](/posts/waerlinx-custom-status-dashboards/#inventory-counts-dashboard)** - displays status of monthly inventory counts.   

* **[Driver Dashboard](/posts/waerlinx-custom-status-dashboards/#driver-dashboard)** - displays status of Order's that are being picked today.  

{{% /portfolio %}}